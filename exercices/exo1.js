// I. Les boucles
/**
 * Exercice 1.1
 * 
 * Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :
 *  - l'afficher
 *  - incrémenter de 1
 */

export const boucle1 = (i) => {
        console.log("exercice 1.1");
        for (i = 0; i < 10; i++) {
            console.log(i);
        }
        console.log("----------------------")
    }
    /**
     * Exercice 1.2
     * 
     * Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. 
     * Tant que la première variable n'est pas supérieur à 20 :
     *
     * - multiplier la première variable avec la deuxième
     * - afficher le résultat
     * - incrémenter la première variable
     */

export const boucle2 = (i, j) => {
        i = 0;
        j = 24;
        console.log("exercice 1.2");
        while (i <= 20) {
            console.log(`${i}*${j} = ${i * j}`);
            i++;
        }
        console.log("----------------------")
    }
    /**
     * Exercice 1.3
     * 
     * Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. 
     * Tant que la première variable n'est pas inférieur ou égale à 10 :
     * - multiplier la première variable avec la deuxième
     * - afficher le résultat
     * - décrémenter la première variable
     */
export const boucle3 = (i, j) => {
        i = 100;
        j = 24;
        console.log("exercice 1.3");
        while (i >= 10) {
            console.log(`${i}*${j} = ${i * j}`);
            i--;
        }
        console.log("----------------------")
    }
    /**
     * Exercice 1.4
     * 
     * Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :
     * - l'afficher
     * - l'incrementer de la moitié de sa valeur
     */
export const boucle4 = (i) => {
        i = 1;
        console.log("exercice 1.4");
        while (i < 10) {
            console.log("1/2 de i = " + i / 2)
            console.log("résultat = " + i);
            i += (i / 2);
        }
        console.log("----------------------")
    }
    /**
     * Exercice 1.5
     * En allant de 1 à 15 avec un pas de 1, afficher le message "On y arrive presque..."
     */
export const boucle5 = (i) => {
        i = 1;
        console.log("exercice 1.5");
        while (i <= 15) {
            console.log(`${i} On y arrive presque ...`)
            i++;
        }
        console.log("----------------------")
    }
    /**
     * Exercice 1.6
     * En allant de 20 à 0 avec un pas de 1, afficher le message "C'est presque bon..."
     */
export const boucle6 = (i) => {
        i = 20;
        console.log("exercice 1.6");
        while (i >= 0) {
            console.log(`${i} C'est presque bon ...`)
            i--;
        }
        console.log("----------------------")
    }
    /**
     * Exercice 1.7
     * En allant de 1 à 100 avec un pas de 15, afficher le message "On tient le bon bout..."
     */
export const boucle7 = (i) => {
        i = 1;
        console.log("exercice 1.7");
        while (i <= 100) {
            console.log(`${i} On tient le bon bout ...`)
            i += 15;
        }
        console.log("----------------------")
    }
    /**
     * Exercice 1.8
     * En allant de 200 à 0 avec un pas de 12, afficher le message "Enfin ! ! !"
     */
export const boucle8 = (i) => {
    i = 200;
    console.log("exercice 1.8");
    while (i >= 0) {
        console.log(`${i} Enfin ! ! !`)
        i -= 12;
    }
    console.log("----------------------")
}

export const callAll = () => {
    boucle1();
    boucle2();
    boucle3();
    boucle4();
    boucle5();
    boucle6();
    boucle7();
    boucle8();
}