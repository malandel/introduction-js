/*II Les fonctions
 *
 * Exercice 2.1 
 * Faire une fonction qui retourne true.
 */

function fonction1(a) {
    console.log("Exercice 2.1")
    a = 0;
    console.log(a == 0);
    console.log("-----------------");
    return (a == 0);
}

/* Exercice 2.2
 * Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.
 */
function fonction2(a) {
    console.log("Exercice 2.2");
    console.log(a);
    console.log("-----------------");
    return a;
}

/* Exercice 2.3
 * Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.
 */
function fonction3(a, b) {
    console.log("Exercice 2.3")
    console.log(a + b);
    console.log("-----------------");
    return a + b;
}

/* Exercice 2.4
 * Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :
 * - "Le premier nombre est plus grand" si le premier nombre est plus grand que le deuxième
 * - "Le premier nombre est plus petit" si le premier nombre est plus petit que le deuxième
 * - "Les deux nombres sont identiques" si les deux nombres sont égaux
 */
function fonction4(a, b) {
    console.log("Exercice 2.4");
    if (a > b) {
        console.log('Le premier nombre est plus grand');
        console.log("-----------------");
        return ('Le premier nombre est plus grand');
    } else if (a < b) {
        console.log('Le premier nombre est plus petit');
        console.log("-----------------");
        return ('Le premier nombre est plus petit');
    } else {
        console.log('Les deux nombres sont identiques');
        console.log("-----------------");
        return ('Les deux nombres sont identiques');
    }
}


/* Exercice 2.5
 * Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.
 */
function fonction5(a, b) {
    console.log("Exercice 2.5");
    console.log(a + " " + b);
    console.log("-----------------");
    return (a + " " + b);
}

/* Exercice 2.6
 * Faire une fonction qui prend trois paramètres : nom, prenom et age. 
 * Elle doit renvoyer une chaine de la forme : "Bonjour" + prenom + nom + ", tu as " + age + " ans". 
 */
function fonction6(nom, prenom, age) {
    console.log("Exercice 2.6");
    console.log("Bonjour " + prenom + " " + nom + ", tu as " + age + " ans");
    console.log("-----------------");
    return ('Bonjour ${prenom} ${nom}, tu as ${age} ans');
}

/* Exercice 2.7
 * Faire une fonction qui prend deux paramètres : age et genre. 
 * Le paramètre genre peut prendre comme valeur Homme ou Femme. 
 * La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :
 * - Vous êtes un homme et vous êtes majeur
 * - Vous êtes un homme et vous êtes mineur
 * - Vous êtes une femme et vous êtes majeur
 * - Vous êtes une femme et vous êtes mineur
 */
function fonction7(age, genre) {
    console.log("Exercice 2.7");
    if (genre === "Femme" || genre === "Homme") {
        if (genre === "Homme") {
            if (age < 18) {
                console.log("Vous êtes un homme et vous êtes mineur.");
                return ("Vous êtes un homme et vous êtes mineur.");
            } else {
                console.log("Vous êtes un homme et vous êtes majeur.");
                return ("Vous êtes un homme et vous êtes majeur.");
            }
        } else if (genre === "Femme") {
            if (age < 18) {
                console.log("Vous êtes une femme et vous êtes mineure.");
                return ("Vous êtes une femme et vous êtes mineure.");
            } else {
                console.log("Vous êtes une femme et vous êtes majeure.");
                return ("Vous êtes une femme et vous êtes majeure.");
            }
        }
    } else {
        console.log("Nous n'acceptons que 'Femme' ou 'Homme' pour le genre.");
        return ("Nous n'acceptons que 'Femme' ou 'Homme' pour le genre.");
    }
    console.log("-------------------");
}

/* Exercice 2.8
 * Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. 
 * Tous les paramètres doivent avoir une valeur par défaut.
 */

function fonction8(a, b, c) {
    console.log("Exercice 2.8");
    a = 12;
    b = 43;
    c = 67;
    console.log(a + b + c);
    console.log("-------------------");
    return a + b + c;
}

export const callAll = () => {
    fonction1();
    fonction2("Je suis une chaine de caractères");
    fonction3("Je suis une ", "concaténation de 2 strings.");
    fonction4(12, 12);
    fonction5(12, " singes");
    fonction6("Dante", "Alighieri", 2020 - 1265);
    fonction7(24, "homme");
    fonction8();
}