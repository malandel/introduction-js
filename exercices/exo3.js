/* III Les tableaux
 */
/* Exercice 3.1
 * Créer un tableau mois et l'initialiser avec le nom des douze mois de l'année.
 */
const mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];

/* Exercice 3.2
 * Avec le tableau de l'exercice 1, afficher la valeur de la troisième ligne de ce tableau.
 */
function exo3_2(array) {
    console.log("exo 3_2");
    console.log("La 3ème ligne est : " + array[2]);
    console.log("------------------");
}

/* Exercice 3.3
 * Avec le tableau de l'exercice 1, afficher la valeur de l'index 5.
 */
function exo3_3(array) {
    console.log("exo 3_3");
    console.log("Le 5ème index est : " + array[5]);
    console.log("------------------");
}

/* Exercice 3.4
 * Avec le tableau de l'exercice 1, modifier le mois de aout pour lui ajouter l'accent manquant.
 */
function exo3_4(array) {
    console.log("exo 3_4");
    array[7] = "Août";
    console.log(array[7])
    console.log("------------------");
}

/* Exercice 3.5
 * Créer un tableau associatif avec comme index le numéro des départements des Hauts de France et en valeur leur nom.
 */
const HautsDeFrance = { "02": "Aisne", "59": "Nord", "60": "Oise", "62": "Pas-de-calais", "80": "Somme" };

/* Exercice 3.6
 * Avec le tableau de l'exercice 5, afficher la valeur de l'index 59.
 */
function exo3_6(array) {
    console.log("exo 3_6");
    console.log(array["59"]);
    console.log("------------------");
}

/* Exercice 3.7
 * Avec le tableau de l'exercice 5, ajouter la ligne correspondant au département de la ville de Reims.
 */
HautsDeFrance["51"] = "Marne";

/* Exercice 3.8
 * Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.
 */
function afficherMois(array) {
    array.forEach(moisUnique => {
        console.log(moisUnique);
    });
    console.log("------------------");
}

/* Exercice 3.9
 * Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.
 */
function afficherHDF(array) {
    for (let index in array) {
        let departement = array[index];
        console.log(departement);
    }
    console.log("------------------");
}

/*Exercice 3.10
 * Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau ainsi que les clés associés. 
 * Cela pourra être, par exemple, de la forme : "Le département" + nom_departement + "a le numéro" + num_departement
 */
function afficherFullHDF(array) {
    for (let index in array) {
        let departement = array[index];
        console.log("Le département " + departement + " a le numéro " + index);
    }
    console.log("------------------");
}

export const callAll = () => {
    exo3_2(mois);
    exo3_3(mois);
    exo3_4(mois);
    exo3_6(HautsDeFrance);
    afficherMois(mois);
    afficherHDF(HautsDeFrance);
    afficherFullHDF(HautsDeFrance);
}