/* III Les classes ES6 */

export class Annee {
    mois = [
        "Janvier",
        "Février",
        "Mars",
        "Avril",
        "Mai",
        "Juin",
        "Juillet",
        "Août",
        "Septembre",
        "Octobre",
        "Novembre",
        "Décembre",
    ];

    /* Exercice 4.2 - Avec le tableau de l'exercice 1, afficher la valeur de la troisième ligne de ce tableau.*/
    exo2() {
        console.log("Exercice 4.2");
        console.log(this.mois[2]);
        console.log("---------------------");
    };
    /* Exercice 4.3 - Avec le tableau de l'exercice 1, afficher la valeur de l'index 5. */
    exo3() {
        console.log("Exercice 4.3");
        console.log(this.mois[5]);
        console.log("---------------------");
    };
    /* Exercice 4.4 - Avec le tableau de l'exercice 1, modifier le mois d'août pour lui ajouter l'accent manquant.*/
    exo4() {
        console.log("Exercice 4.4");
        this.mois[7] = "Août";
        console.log(this.mois[7]);
        console.log("---------------------");
    };
    /* Exercice 4.8 - Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.*/
    exo8() {
        console.log("Exercice 4.8");
        this.mois.forEach(moisUnique => {
            console.log(moisUnique);
        });
        console.log("------------------");
    }
}

/* Exercice 4.5 - Créer un tableau associatif avec comme index le numéro des départements des Hauts de France et en valeur leur nom.*/

export class HDF {
    departements = {
        "02": "Aisne",
        "59": "Nord",
        "60": "Oise",
        "62": "Pas-de-calais",
        "80": "Somme"
    };
    /* Exercice 4.6 - Avec le tableau de l'exercice 5, afficher la valeur de l'index 59.*/
    exo6() {
        console.log("Exercice 4.6");
        console.log(this.departements["59"]);
        console.log("---------------------");
    };
    /* Exercice 4.7 - Avec le tableau de l'exercice 5, ajouter la ligne correspondant au département de la ville de Reims.*/
    exo7() {
        console.log("Exercice 4.7");
        this.departements["51"] = "Marne";
        console.log(this.departements["51"]);
        console.log("---------------------");
    };
    /* Exercice 4.9 - Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.*/
    exo9() {
            console.log("Exercice 4.9");
            for (let index in this.departements) {
                let departementUnique = this.departements[index];
                console.log(departementUnique);
            }
            console.log("------------------");
        }
        /*Exercice 4.10 - Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau ainsi que les clés associés. 
         * Cela pourra être : "Le département" + nom_departement + "a le numéro" + num_departement */
    exo10() {
        console.log("Exercice 4.10");
        for (let index in this.departements) {
            let departementUnique = this.departements[index];
            console.log("Le département " + departementUnique + " a le numéro " + index)
        }

    }


}