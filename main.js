import { callAll as callAll1 } from './exercices/exo1.js'
callAll1();

import { callAll as callAll2 } from './exercices/exo2.js'
callAll2();

import { callAll as callAll3 } from './exercices/exo3.js'
callAll3();

import { Annee } from './exercices/exo4.js'
let annee = new Annee();
annee.exo2();
annee.exo3();
annee.exo4();
annee.exo8();

import { HDF } from './exercices/exo4.js'
let hdf = new HDF();
hdf.exo6();
hdf.exo7();
hdf.exo9();
hdf.exo10();